# go-proj4
[PROJ.4](https://github.com/OSGeo/proj.4) binding for go with PROJ.4 embeded.

If you need to use other init/grid files, please use environment variable `PROJ_LIB`.

# LICENSE
MIT, see LICENSE file
