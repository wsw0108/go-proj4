package proj4

//go:generate go-bindata -pkg proj4 -ignore README|CMakeLists\.txt|install\.in|nad\.lst|[Mm]akefile.*|test.*|.*\.dist$ -prefix proj.4/nad proj.4/nad

/*
#include "fileapi.h"
*/
import "C"
import "unsafe"

//export goNameInAssets
func goNameInAssets(filename *C.char) C.int {
	name := C.GoString(filename)
	names := AssetNames()
	for i := range names {
		if name == names[i] {
			return 1
		}
	}
	return 0
}

//export goBindataFopen
func goBindataFopen(ctx C.projCtx, filename *C.char, access *C.char) C.PAFile {
	name := C.GoString(filename)
	if data, err := Asset(name); err == nil {
		buf := unsafe.Pointer(&data[0])
		len := C.long(len(data))
		pafile := C.new_bindata_pafile(buf, len)
		return pafile
	}
	return nil
}

func setCustomFileAPI(ctx C.projCtx) {
	C.set_custom_fileapi(ctx)
}
