package proj4

//go:generate go run gen.go

/*
#cgo CFLAGS: -Iproj.4/src
#cgo windows CFLAGS: -Iconfig -DMUTEX_win32
#cgo !windows CFLAGS: -Iconfig -DMUTEX_pthread -DHAVE_PTHREAD_MUTEX_RECURSIVE
#cgo !windows LDFLAGS: -lm -lpthread

#include <proj_api.h>
#include <stdlib.h>
*/
import "C"

import (
	"errors"
	"math"
	"runtime"
	"strings"
	"unsafe"
)

// Proj4 represents a proj4 style projection
type Proj4 struct {
	pj        C.projPJ
	ctx       C.projCtx
	isLatLong bool
}

const (
	deg2Rad = math.Pi / 180.0
	rad2Deg = 180.0 / math.Pi
)

// NewProj4 create new projection from proj4 string (e.g. "+proj=longlat +datum=WGS84 +no_defs").
func NewProj4(def string) (*Proj4, error) {
	ctx := C.pj_ctx_alloc()
	if ctx == nil {
		ref := C.pj_get_errno_ref()
		if ref == nil {
			return nil, errors.New("unknown error on pj_ctx_alloc")
		}
		return nil, errors.New(C.GoString(C.pj_strerrno(*ref)))
	}

	setCustomFileAPI(ctx)

	c := C.CString(def)
	defer C.free(unsafe.Pointer(c))

	pj := C.pj_init_plus_ctx(ctx, c)
	if pj == nil {
		errno := C.pj_ctx_get_errno(ctx)
		return nil, errors.New(C.GoString(C.pj_strerrno(errno)))
	}

	proj := &Proj4{pj: pj, ctx: ctx}
	proj.isLatLong = (C.pj_is_latlong(proj.pj) != 0)

	runtime.SetFinalizer(proj, free)

	return proj, nil
}

func free(p *Proj4) {
	p.Free()
}

// Free deallocates the projection.
func (p *Proj4) Free() {
	if p.pj != nil {
		C.pj_free(p.pj)
		p.pj = nil
	}
	if p.ctx != nil {
		C.pj_ctx_free(p.ctx)
		p.ctx = nil
	}
}

// Forward forward point.
func (p *Proj4) Forward(lng, lat float64) (x, y float64, err error) {
	if p.isLatLong {
		return lng, lat, nil
	}
	lng *= deg2Rad
	lat *= deg2Rad
	lp := C.projUV{u: C.double(lng), v: C.double(lat)}
	xy := C.pj_fwd(lp, p.pj)
	x, y = float64(xy.u), float64(xy.v)
	errno := C.pj_ctx_get_errno(p.ctx)
	if errno != 0 {
		err = errors.New(C.GoString(C.pj_strerrno(errno)))
		return
	}
	return
}

// Inverse inverse point.
func (p *Proj4) Inverse(x, y float64) (lng, lat float64, err error) {
	if p.isLatLong {
		return x, y, nil
	}
	xy := C.projUV{u: C.double(x), v: C.double(y)}
	lp := C.pj_inv(xy, p.pj)
	lng, lat = float64(lp.u), float64(lp.v)
	errno := C.pj_ctx_get_errno(p.ctx)
	if errno != 0 {
		err = errors.New(C.GoString(C.pj_strerrno(errno)))
		return
	}
	lng *= rad2Deg
	lat *= rad2Deg
	return
}

func buildTransformError(src, dst *Proj4) error {
	errno1 := C.pj_ctx_get_errno(src.ctx)
	errno2 := C.pj_ctx_get_errno(dst.ctx)
	if errno1 == 0 && errno2 == 0 {
		return errors.New("unknown error")
	}
	a := make([]string, 0)
	if errno1 != 0 {
		a = append(a, C.GoString(C.pj_strerrno(errno1)))
	}
	if errno2 != 0 {
		a = append(a, C.GoString(C.pj_strerrno(errno2)))
	}
	return errors.New(strings.Join(a, " / "))
}

// TransformPoint transform point to dst projection in place.
func (p *Proj4) TransformPoint(dst *Proj4, x, y *float64) error {
	if p.isLatLong {
		*x *= deg2Rad
		*y *= deg2Rad
	}

	status := C.pj_transform(p.pj, dst.pj, 1, 0,
		(*C.double)(unsafe.Pointer(x)),
		(*C.double)(unsafe.Pointer(y)),
		nil)
	if status != 0 {
		return buildTransformError(p, dst)
	}

	if dst.isLatLong {
		*x *= rad2Deg
		*y *= rad2Deg
	}

	return nil
}

// Transform transform points to dst projection in place.
func (p *Proj4) Transform(dst *Proj4, xs, ys []float64) error {
	if len(xs) != len(ys) {
		return errors.New("number of x and y coordinates differs")
	}
	if xs == nil || ys == nil {
		return nil
	}

	if p.isLatLong {
		for i := range xs {
			xs[i] *= deg2Rad
		}
		for i := range ys {
			ys[i] *= deg2Rad
		}
	}

	status := C.pj_transform(p.pj, dst.pj, C.long(len(xs)), 0,
		(*C.double)(unsafe.Pointer(&xs[0])),
		(*C.double)(unsafe.Pointer(&ys[0])),
		nil)
	if status != 0 {
		return buildTransformError(p, dst)
	}

	if dst.isLatLong {
		for i := range xs {
			xs[i] *= rad2Deg
		}
		for i := range ys {
			ys[i] *= rad2Deg
		}
	}

	return nil
}

// IsLatLong return true if under proj is geographic
func (p *Proj4) IsLatLong() bool {
	return p.isLatLong
}
