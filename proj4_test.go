package proj4

import (
	"math"
	"strings"
	"testing"
)

func TestForwardGeographic(t *testing.T) {
	p, err := NewProj4("+init=epsg:4326")
	if err != nil {
		t.Fatal(err)
	}
	defer p.Free()

	x0, y0 := 120.0, 30.0

	x1, y1, err := p.Forward(x0, y0)
	if err != nil {
		t.Fatal(err)
	}
	if math.Abs(x1-120.0) > 1e-7 {
		t.Error(x1)
	}
	if math.Abs(y1-30.0) > 1e-7 {
		t.Error(y1)
	}

}

func TestInverseGeographic(t *testing.T) {
	p1, err := NewProj4("+init=epsg:4326")
	if err != nil {
		t.Fatal(err)
	}
	defer p1.Free()

	x0, y0 := 120.0, 30.0

	x1, y1, err := p1.Inverse(x0, y0)
	if err != nil {
		t.Fatal(err)
	}
	if math.Abs(x1-120.0) > 1e-7 {
		t.Error(x1)
	}
	if math.Abs(y1-30.0) > 1e-7 {
		t.Error(y1)
	}
}

func TestForward(t *testing.T) {
	p, err := NewProj4("+init=epsg:3857")
	if err != nil {
		t.Fatal(err)
	}
	defer p.Free()

	x0, y0 := 120.0, 30.0

	x1, y1, err := p.Forward(x0, y0)
	if err != nil {
		t.Fatal(err)
	}
	if math.Abs(x1-13358338.895192828) > 1e-3 {
		t.Error(x1)
	}
	if math.Abs(y1-3503549.843504374) > 1e-3 {
		t.Error(y1)
	}
}

func TestInverse(t *testing.T) {
	p, err := NewProj4("+init=epsg:3857")
	if err != nil {
		t.Fatal(err)
	}
	defer p.Free()

	x0, y0 := 120.0, 30.0
	x0, y0, _ = p.Forward(x0, y0)

	x1, y1, err := p.Inverse(x0, y0)
	if err != nil {
		t.Fatal(err)
	}
	if math.Abs(x1-120.0) > 1e-7 {
		t.Error(x1)
	}
	if math.Abs(y1-30.0) > 1e-7 {
		t.Error(y1)
	}
}

func TestTransformPoint(t *testing.T) {
	p1, err := NewProj4("+init=epsg:4326")
	if err != nil {
		t.Fatal(err)
	}
	defer p1.Free()
	p2, err := NewProj4("+init=epsg:3857")
	if err != nil {
		t.Fatal(err)
	}
	defer p2.Free()

	x := 120.0
	y := 30.0

	if err := p1.TransformPoint(p2, &x, &y); err != nil {
		t.Fatal(err)
	}

	if math.Abs(x-13358338.895192828) > 1e-3 {
		t.Error(x)
	}
	if math.Abs(y-3503549.843504374) > 1e-3 {
		t.Error(y)
	}

	if err := p2.TransformPoint(p1, &x, &y); err != nil {
		t.Fatal(err)
	}

	if math.Abs(x-120.0) > 1e-7 {
		t.Error(x)
	}
	if math.Abs(y-30.0) > 1e-7 {
		t.Error(y)
	}
}

func TestTransform(t *testing.T) {
	p1, err := NewProj4("+init=epsg:4326")
	if err != nil {
		t.Fatal(err)
	}
	defer p1.Free()
	p2, err := NewProj4("+init=epsg:3857")
	if err != nil {
		t.Fatal(err)
	}
	defer p2.Free()

	xs := []float64{120.0}
	ys := []float64{30.0}

	if err := p1.Transform(p2, xs, ys); err != nil {
		t.Fatal(err)
	}

	if math.Abs(xs[0]-13358338.895192828) > 1e-3 {
		t.Error(xs)
	}
	if math.Abs(ys[0]-3503549.843504374) > 1e-3 {
		t.Error(ys)
	}

	if err := p2.Transform(p1, xs, ys); err != nil {
		t.Fatal(err)
	}

	if math.Abs(xs[0]-120.0) > 1e-7 {
		t.Error(xs)
	}
	if math.Abs(ys[0]-30.0) > 1e-7 {
		t.Error(ys)
	}
}

func TestTransformErrors(t *testing.T) {
	p1, err := NewProj4("+init=epsg:4326")
	defer p1.Free()
	if err != nil {
		t.Fatal(err)
	}

	p2, err := NewProj4("+init=epsg:3857")
	defer p2.Free()
	if err != nil {
		t.Fatal(err)
	}

	xs := []float64{120.0}
	ys := []float64{91}
	if err := p1.Transform(p2, xs, ys); !strings.Contains(err.Error(), "latitude or longitude exceeded limits") {
		t.Error("unexpected error when transforming:", err)
	}
}

func BenchmarkTransform(b *testing.B) {
	xs := []float64{120.1234, 121.2345, 123.4567, 124.5678}
	ys := []float64{30.1234, 31.2345, 33.4567, 34.5678}

	p1, err := NewProj4("+init=epsg:4326")
	defer p1.Free()
	if err != nil {
		b.Fatal(err)
	}

	p2, err := NewProj4("+init=epsg:3857")
	defer p2.Free()
	if err != nil {
		b.Fatal(err)
	}

	for i := 0; i < b.N; i++ {
		if err := p1.Transform(p2, xs, ys); err != nil {
			b.Fatal(err)
		}
		if err := p2.Transform(p1, xs, ys); err != nil {
			b.Fatal(err)
		}
	}
}
