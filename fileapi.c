#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "_cgo_export.h"


typedef struct {
    void *buf;
    long len;
    long pos;
} bindata_pafile;

typedef struct {
    int a;
    projFileAPI *d;
    union {
        PAFile df;
        PAFile bf;
    } u;
} custom_pafile;


/* bindata，read file from go-bindata assets */
PAFile new_bindata_pafile(void *buf, long len) {
    bindata_pafile *pafile;

    pafile = malloc(sizeof(bindata_pafile));
    if (pafile == NULL) {
        return NULL;
    }
    pafile->buf = malloc(len);
    if (pafile->buf == NULL) {
        free(pafile);
        return NULL;
    }
    memcpy(pafile->buf, buf, len);
    pafile->len = len;
    pafile->pos = 0;

    return (PAFile)pafile;
}

static PAFile bindata_fopen(projCtx ctx, const char *filename, const char *access)
{
    return goBindataFopen(ctx, (char *)filename, (char *)access);
}

static size_t bindata_fread(void *buffer, size_t size, size_t nmemb, PAFile file)
{
    bindata_pafile *pafile = (bindata_pafile *)file;

    if (size == 0 || nmemb == 0) {
        return 0;
    }

    if (pafile->pos >= pafile->len) {
        return 0;
    }

    size_t len = size * nmemb;
    size_t remain = pafile->len - pafile->pos;
    if (len > remain) {
        len = remain;
    }

    memcpy(buffer, pafile->buf + pafile->pos, len);
    pafile->pos += len;

    return len / size;
}

static int bindata_fseek(PAFile file, long offset, int whence)
{
    bindata_pafile *pafile = (bindata_pafile *)file;

    int ret = -1;
    if (whence == SEEK_SET) {
        if (offset < pafile->len) {
            pafile->pos = offset;
            ret = 0;
        }
    } else if (whence == SEEK_CUR) {
        if (pafile->pos + offset < pafile->len) {
            pafile->pos += offset;
            ret = 0;
        }
    }

    return ret;
}

static long bindata_ftell(PAFile file)
{
    bindata_pafile *pafile = (bindata_pafile *)file;
    return pafile->pos;
}

static void bindata_fclose(PAFile file)
{
    bindata_pafile *pafile = (bindata_pafile *)file;
    free(pafile->buf);
    free(pafile);
}

/* custom, read file from file-system(stdio) extra */
static PAFile custom_fopen(projCtx ctx, const char *filename, const char *access)
{
    custom_pafile *pafile;

    pafile = malloc(sizeof(custom_pafile));
    if (pafile == NULL) {
        return NULL;
    }

    pafile->d = pj_get_default_fileapi();

    int name_in_assets = goNameInAssets((char *)filename);
    PAFile f = NULL;
    if (name_in_assets) {
        pafile->a = 1;
        f = bindata_fopen(ctx, filename, access);
        if (f == NULL) {
            free(pafile);
            return NULL;
        }
        pafile->u.bf = f;
    } else {
        pafile->a = 0;
        f = pafile->d->FOpen(ctx, filename, access);
        if (f == NULL) {
            free(pafile);
            return NULL;
        }
        pafile->u.df = f;
    }

    return (PAFile)pafile;
}

static size_t custom_fread(void *buffer, size_t size, size_t nmemb, PAFile file)
{
    custom_pafile *pafile = (custom_pafile *)file;
    if (pafile->a) {
        return bindata_fread(buffer, size, nmemb, pafile->u.bf);
    } else {
        return pafile->d->FRead(buffer, size, nmemb, pafile->u.df);
    }
}

static int custom_fseek(PAFile file, long offset, int whence)
{
    custom_pafile *pafile = (custom_pafile *)file;
    if (pafile->a) {
        return bindata_fseek(pafile->u.bf, offset, whence);
    } else {
        return pafile->d->FSeek(pafile->u.df, offset, whence);
    }
}

static long custom_ftell(PAFile file)
{
    custom_pafile *pafile = (custom_pafile *)file;
    if (pafile->a) {
        return bindata_ftell(pafile->u.bf);
    } else {
        return pafile->d->FTell(pafile->u.df);
    }
}

static void custom_fclose(PAFile file)
{
    custom_pafile *pafile = (custom_pafile *)file;
    if (pafile->a) {
        bindata_fclose(pafile->u.bf);
    } else {
        pafile->d->FClose(pafile->u.df);
    }
    free(file);
}

static projFileAPI custom_fileapi = {
    custom_fopen,
    custom_fread,
    custom_fseek,
    custom_ftell,
    custom_fclose
};

void set_custom_fileapi(projCtx ctx)
{
    pj_ctx_set_fileapi(ctx, &custom_fileapi);
}
